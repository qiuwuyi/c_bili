const test = {
	state: {
		list: [],
		str1: '',
		str2: '',
	},
	mutations: {
		set_test_list(state, arr) {
			state.list = arr
		},
		set_test_str1(state, n) {
			state.str1 = n
		},
		set_test_str2(state, n) {
			state.str2 = n
		},
	},
}
export {test}
