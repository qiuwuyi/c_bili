module.exports = {
	chainWebpack: config => {
		// 路径别名 src/someTest
		config.resolve.alias.set('@test', config.resolve.alias.get('@') + '/someTest')
	},
}
