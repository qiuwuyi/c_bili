import axios from 'axios'
import Vue from 'vue'
const qs = require('qs')

const urls = {
	local: '',
	onLine: 'https://...',
}
const baseURL = urls.local

let ax0 = axios.create({
	baseURL,
	headers: {
		// 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
	},
	method: 'POST',
	// withCredentials: true,  让后台可以使用session
})

// 设置过滤器
ax0.interceptors.request.use(
	function(config) {
		if (localStorage.token) {
			config.headers.Authorization = localStorage.token
		}
		return config
	},
	function(error) {
		return Promise.reject(error)
	},
)
ax0.interceptors.response.use(
	function(response) {
		const re = response.data
		return re
	},
	function(e) {
		return {code: 0}
	},
)
// 封装使用时统一
const vpost = (link, params) => {
	const s = qs.stringify(params)
	return ax0.post(link, s)
}
const vget = (link, params) => {
	const s = {params}
	return ax0.get(link, s)
}

Vue.prototype.$qpost = vpost
Vue.prototype.$qget = vget

export {vpost, vget}
