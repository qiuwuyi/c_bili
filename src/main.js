import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import './registerServiceWorker'
import './axios'
import './style'
import './component/use.js'
import './qsoftui'
import Toast from './qsoftui/toast'
Vue.use(Toast)

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app')
