import Vue from 'vue'

// 按钮-弹窗, 鼠标移入会就近显示一个小弹窗
Vue.component('c-btn-dialog', () => import('./BtnDialog.vue'))
Vue.component('c-small-video', () => import('./SmallVideobox.vue'))
